using System;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEditor;

namespace SetParentEditor
{
    public enum MaterialSwapMode
    {
        Name,
        Index,
        Reference,
    }
    public class MaterialSwapper : EditorWindow
    {
        [SerializeField] private GameObject _parentObject;
        [SerializeField] private string _materialName;
        [SerializeField] private int _materialIndex;
        [SerializeField] private Material _oldMaterial;
        [SerializeField] private Material _newMaterial;
        //[SerializeField] private bool _worldSpace;

        private List<Renderer> _renderers = new();
        private MaterialSwapMode _swapMode;

        [MenuItem("Window/Custom Tools/Material Swapper")]
        public static void ShowWindow()
        {
            EditorWindow.GetWindow<MaterialSwapper>("Material Swapper");
        }
    
        private void OnGUI()
        {
            _parentObject = (GameObject) EditorGUILayout.ObjectField("Parent Object", _parentObject, typeof(GameObject), true);
            if (!_parentObject || _parentObject.transform.childCount < 1)
                return;

            _swapMode = (MaterialSwapMode) EditorGUILayout.EnumPopup("Swap mode", _swapMode);
            switch (_swapMode)
            {
                case MaterialSwapMode.Name:
                    _materialName = EditorGUILayout.TextField("Old Material", _materialName);
                    break;
                case MaterialSwapMode.Index:
                    _materialIndex = EditorGUILayout.IntField("Old Material", _materialIndex);
                    break;
                case MaterialSwapMode.Reference:
                    _oldMaterial = (Material) EditorGUILayout.ObjectField("Old Material", _newMaterial, typeof(Material), false);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            _newMaterial = (Material) EditorGUILayout.ObjectField(("New Material"), _newMaterial, typeof(Material), false);

            if (GUILayout.Button("Swap Materials"))
            {
                _renderers = _parentObject.GetComponentsInChildren<Renderer>().ToList();

                for (int i = 0; i < _renderers.Count; i++)
                {
                    var r = _renderers[i];
                    var index = r.sharedMaterials.ToList().FindIndex(m => m.name == _materialName);
                    
                    if(index == -1)
                        continue;

                    var newMaterials = new Material[r.sharedMaterials.Length];
                    for (int j = 0; j < newMaterials.Length; j++)
                    {
                        newMaterials[j] = j == index ? _newMaterial : r.sharedMaterials[j];
                    }

                    r.sharedMaterials = newMaterials;
                }
            }
        }
    }
}
