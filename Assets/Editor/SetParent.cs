using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace SetParentEditor
{
    public class SetParent : EditorWindow
    {
        [SerializeField] private GameObject _object;
        [SerializeField] private bool _applyToChildren;
        [SerializeField] private bool _enumerateNames;
        [SerializeField] private string _nameOfParent;
        [SerializeField] private Vector3 _offset;
        [SerializeField] private Vector3 _rotation;
        [SerializeField] private bool _worldSpace;
        [SerializeField] private bool _useObjectCenter;

        private List<Transform> _transforms = new();
    
        [MenuItem("Window/Custom Tools/Create Empty Parent")]
        public static void ShowWindow()
        {
            EditorWindow.GetWindow<SetParent>("Create Empty Parent");
        }
    
        private void OnGUI()
        {
            _object = (GameObject) EditorGUILayout.ObjectField("Object", _object, typeof(GameObject), true);
            if (!_object)
                return;

            _nameOfParent = EditorGUILayout.TextField("Parent Name", _nameOfParent);
            if (_object.transform.childCount > 0)
            {
                _applyToChildren = EditorGUILayout.Toggle("Apply To Children", _applyToChildren);
                if(_applyToChildren)
                    _enumerateNames = EditorGUILayout.Toggle("Enumerate names", _enumerateNames);
            }
            
            _offset = EditorGUILayout.Vector3Field("Offset", _offset);
            _rotation = EditorGUILayout.Vector3Field("Rotation", _rotation);
            _worldSpace = EditorGUILayout.Toggle("World Space", _worldSpace);
            _useObjectCenter = EditorGUILayout.Toggle("Use Mesh Center", _useObjectCenter);

            if (_object && GUILayout.Button("Create empty parent at pivot"))
            {
                _transforms.Clear();
                if (_applyToChildren)
                {
                    var t = _object.transform;
                    for (int i = 0; i < t.childCount; i++)
                    {
                        _transforms.Add(t.GetChild(i));
                    }
                }
                else
                {
                    _transforms.Add(_object.transform);
                }

                for (int i = 0; i < _transforms.Count; i++)
                {
                    var suffix = _applyToChildren ? (_enumerateNames ? $" ({i+1})" : "") : "";
                    
                    CreateParent(_transforms[i], _nameOfParent + suffix);
                }
            }
        }

        private void CreateParent(Transform objectTransform, string parentName)
        {
            var newParent = new GameObject();
            newParent.name = parentName;

            if (!_worldSpace)
            {
                newParent.transform.SetParent(objectTransform);
                newParent.transform.localPosition = _offset;
                newParent.transform.localRotation = Quaternion.Euler(_rotation.x, _rotation.y, _rotation.z);
            }
            else
            {
                if (_useObjectCenter)
                {
                    var renderer = objectTransform.GetComponentInChildren<Renderer>();
                    if (!renderer)
                    {
                        Debug.LogError($"No renderer found under object {objectTransform.name}");
                        return;
                    }

                    
                    newParent.transform.position = renderer.bounds.center + _offset;
                }
                else
                {
                    newParent.transform.position = objectTransform.position + _offset;
                }
                
                newParent.transform.rotation = Quaternion.Euler(_rotation.x, _rotation.y, _rotation.z);
            }

            if(_applyToChildren)
                newParent.transform.SetParent(_object.transform);
            
            objectTransform.SetParent(newParent.transform);

        }
    }
}
