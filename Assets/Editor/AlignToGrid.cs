using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using UnityEngine.Serialization;

namespace SetParentEditor
{
    public class AlignToGrid : EditorWindow
    {
        [SerializeField] private GameObject _parentObject;
        [SerializeField] private int _columns = 3;
        [SerializeField] private Vector3 _spacing;
        [SerializeField] private bool _sortByPosition;
        [SerializeField] private bool _enumerateSorted;
        //[SerializeField] private bool _worldSpace;

        private List<Transform> _transforms = new();

        [MenuItem("Window/Custom Tools/Align To Grid")]
        public static void ShowWindow()
        {
            EditorWindow.GetWindow<AlignToGrid>("Align to Grid");
        }
    
        private void OnGUI()
        {
            _parentObject = (GameObject) EditorGUILayout.ObjectField("Parent Object", _parentObject, typeof(GameObject), true);
            if (!_parentObject || _parentObject.transform.childCount < 1)
                return;

            _columns = EditorGUILayout.IntField("Columns", _columns);
            _spacing = EditorGUILayout.Vector3Field("Spacing", _spacing);
            _sortByPosition = EditorGUILayout.Toggle("Sort by position", _sortByPosition);
            _enumerateSorted = EditorGUILayout.Toggle("(Re)Enumerate objects", _enumerateSorted);

            if (GUILayout.Button("Align children to Grid"))
            {
                _transforms.Clear();
                var childCount = _parentObject.transform.childCount;
                var rows = childCount / _columns;

                var startOffset = new Vector3(-0.5f * _columns * _spacing.x, -0.5f * rows * _spacing.y, -0.5f * rows * _spacing.z);
                
                for (int i = 0; i < _parentObject.transform.childCount; i++)
                {
                    _transforms.Add(_parentObject.transform.GetChild(i));
                }

                if (_sortByPosition)
                {
                    _transforms = _transforms.OrderBy(t => t.position.z).ToList();
                    
                    List<Transform> _newList = new List<Transform>();
                    for (int r = 0; r < rows; r++)
                    {
                        var subset = _transforms.GetRange(r * _columns, _columns)
                            .OrderBy(t => t.position.x).ToList();
                        _newList.AddRange(subset);
                    }
                    
                    _transforms = _newList;
                }

                for (int i = 0; i < _transforms.Count; i++)
                {
                    var t = _transforms[i];
                    t.localPosition = startOffset + 
                                      new Vector3(i % _columns * _spacing.x, (i / _columns) * _spacing.y, (i / _columns) * _spacing.z);
                    
                    if(_sortByPosition)
                        t.SetSiblingIndex(i);

                    if (!_enumerateSorted)
                        continue;
                    
                    var index = t.name.LastIndexOf('(');
                    var index2 = t.name.LastIndexOf(')');
                    
                    if(index != -1 && index2 != -1 && (index2 - index >= 1)
                       && t.name.Substring(index+1, index2-index-1).All(s => s is >= '0' and <= '9'))
                        t.name = $"{t.name.Substring(0, index)}({i + 1})";
                    else
                        t.name = $"{t.name} ({i + 1})";
                }
            }
        }
    }
}
