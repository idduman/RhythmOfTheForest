using DG.Tweening;
using TMPro;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;

namespace Witmina_rotf
{
    public class FeedbackController : MonoBehaviour
    {
        [SerializeField] private TMP_Text _goText;

        private Sequence _showSequence;

        public void Initialize()
        {
            _goText.gameObject.SetActive(false);
        }

        public void ShowFeedback(string text, float duration)
        {
            _showSequence.Kill();

            _goText.text = text;
            _goText.gameObject.SetActive(true);
            _goText.transform.localScale = 0.3f * Vector3.one;
            _goText.color = Color.white;
            _showSequence = DOTween.Sequence().SetEase(Ease.Linear)
                .OnComplete(() => _goText.gameObject.SetActive(false));

            _showSequence.Append(_goText.transform.DOScale(Vector3.one, duration)
                .SetEase(Ease.InQuad));
            _showSequence.Insert(0.8f*duration, _goText.DOFade(0.2f, 0.4f * duration)
                .SetEase(Ease.InQuad));
            _showSequence.Play();
        }
    }

}
