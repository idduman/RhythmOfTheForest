using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Witmina_rotf
{
    public class UIController : MonoBehaviour
    {
        [SerializeField] private RectTransform _successPanel;
        [SerializeField] private RectTransform _failPanel;
        [SerializeField] private Animator _transition;
        
        private static readonly int Transition1 = Animator.StringToHash("Transition");

        public void Initialize()
        {
            _successPanel.gameObject.SetActive(false);
            _failPanel.gameObject.SetActive(false);
        }

        public void Reload()
        {
            GameManager.Instance.Load();
        }

        public void ShowEndGamePanel(bool success)
        {
            if(success)
                _successPanel.gameObject.SetActive(true);
            else
                _failPanel.gameObject.SetActive(true);
        }

        public void Transition()
        {
            _transition.SetTrigger(Transition1);
        }
    }
}

