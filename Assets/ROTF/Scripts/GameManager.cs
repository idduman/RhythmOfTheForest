using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Witmina_rotf
{
    public class GameManager : SingletonBehaviour<GameManager>
    {
        [SerializeField] private UIController _uiController;
        [SerializeField] private LevelBehaviour _level;
        [SerializeField] private int _maxLevel = 4;
        [SerializeField] private int _maxTries = 10;

        public int MaxLevel => _maxLevel;

        private int _trialCount;

        private int _playerLevel;
        public int PlayerLevel {
            get => _playerLevel;
            private set
            {
                _playerLevel = Math.Clamp(value, 1, _maxLevel);
                PlayerPrefs.SetInt("RhythmOfTheForest_PlayerLevel", _playerLevel);
            }
        }

        private void Start()
        {
            PlayerLevel = PlayerPrefs.GetInt("RhythmOfTheForest_PlayerLevel", 1);
            ResetData();
            Load();
        }

        public void Load()
        {
            _uiController.Initialize();
            _level.LoadLevel(PlayerLevel);
        }

        private void ResetData()
        {
            _trialCount = 0;
        }

        public void FinishLevel(bool success, bool isWater = false)
        {
            _trialCount++;
            _level.OnFinish();
            if (success)
            {
                PlayerLevel++;
            }
            else
            {
                PlayerLevel--;
            }

            if (_trialCount >= _maxTries)
            {
                EndGame();
                return;
            }

            StartCoroutine(ReloadRoutine());
        }

        private void EndGame()
        {
            _uiController.ShowEndGamePanel(true);
        }

#region Coroutines

        private IEnumerator ReloadRoutine()
        {
            _uiController.Transition();
            yield return new WaitForSeconds(2.5f);
            Load();
        }

#endregion
    }
}

