using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Witmina_rotf
{
    [CreateAssetMenu(menuName = MenuName)]
    public class LevelData : ScriptableObject
    {
        private const string MenuName = "Data/RhythmOfForest/LevelData";

        [SerializeField] private List<Object> _levelData;

        public Object GetLevelData(int playerLevel)
        {
            return _levelData[playerLevel % _levelData.Count];
        }
    }
}

