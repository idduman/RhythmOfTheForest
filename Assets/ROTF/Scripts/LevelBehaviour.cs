using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Witmina_rotf
{
    public class LevelBehaviour : MonoBehaviour
    {
#region Fields
        [SerializeField] private int _sequenceId;
        [SerializeField] private int _sequenceTake;
        [SerializeField] private bool _recordMode;
        [SerializeField] private bool _debugMode;

        private FeedbackController _feedback;
        private MushroomController _mushroomController;
        private BarController _barController;

        private List<SequenceElement> _sequence = new();
        private List<SequenceElement> _playerSequence = new();

        private float _timer;
        private bool _sequencePlaying;
        private bool _completed;
        private bool _timerRunning;
        private bool _success;
        private int _currentTake;

        private Coroutine _playRoutine;
        
        private static readonly float StartDelay = 0.6f;
#endregion

#region Unity Methods

        private void Awake()
        {
            _currentTake = Random.Range(0, 6);
        }

#if UNITY_EDITOR
        private void Update()
        {
            if(_recordMode && Input.GetKeyDown(KeyCode.Space))
                SaveSequence(_sequenceId, _sequenceTake);

            if (Input.GetKeyDown(KeyCode.Q))
                MushroomOnPressed(0);
            if (Input.GetKeyDown(KeyCode.W))
                MushroomOnPressed(1);
            if (Input.GetKeyDown(KeyCode.E))
                MushroomOnPressed(2);
            if (Input.GetKeyDown(KeyCode.R))
                MushroomOnPressed(3);
            
            if (Input.GetKeyDown(KeyCode.P))
            {
                if (_playRoutine != null)
                {
                    StopCoroutine(_playRoutine);
                    _sequencePlaying = false;
                }
                _playRoutine = StartCoroutine(PlaySequence());
            }
        }
#endif

        private void FixedUpdate()
        {
            if (_timerRunning)
            {
                _timer+=Time.fixedDeltaTime;
                if(_barController)
                    _barController.SetTimerLine(_timer);
            }
        }

#endregion

#region Public Methods
        public void LoadLevel(int playerLevel)
        {
            _mushroomController = GetComponentInChildren<MushroomController>();
            if (!_mushroomController)
            {
                Debug.LogError($"No mushroom controller found in level {name}");
                return;
            }
            
            _barController = GetComponentInChildren<BarController>();
            if (!_barController)
            {
                Debug.LogError($"No bar controller found in level {name}");
                return;
            }
            
            _feedback = GetComponentInChildren<FeedbackController>();
            if (!_feedback)
            {
                Debug.LogError($"No feedback controller found in level {name}");
                return;
            }

            _feedback.Initialize();
            _barController.Clear();
            _completed = false;
            Subscribe();
            
#if UNITY_EDITOR
            if (_recordMode)
                return;
#endif

            _currentTake = Random.Range(0, 6);
            LoadSequence((playerLevel-1) % GameManager.Instance.MaxLevel, _currentTake);
        }
#endregion
        
#region Helpers
        private void StartTimer()
        {
            _timer = 0;
            _timerRunning = true;
        }

        private void SaveSequence(int id, int take)
        {
            FileHandler.SaveToJSON(_sequence, $"Sequence_{id}-{take}.json");
        }

        private void LoadSequence(int id, int take)
        {
            var file = Resources.Load<TextAsset>(
                $"ROTF/Sequences/Sequence_{(_debugMode ? _sequenceId : id)}-{(_debugMode ? _sequenceTake : take)}")
                .ToString();
            _sequence = JsonHelper.FromJson<SequenceElement> (file).ToList ();
            Debug.Log($"Sequence loaded id: {id}");

            if (_playRoutine != null)
            {
                StopCoroutine(_playRoutine);
            }
            _sequencePlaying = false;
            _barController.SetBar(_sequence);
            _barController.Active = true;
            _playRoutine = StartCoroutine(PlaySequence());

        }

        private void MushroomOnPressed(int index)
        {
            
            _mushroomController.PlayMushroom(index);
#if UNITY_EDITOR
            if (_recordMode)
            {
                if(_sequence.Count < 1)
                    StartTimer();
                
                _sequence.Add(new SequenceElement(index, _timer));
                return;
            }
#endif
            if(_playerSequence.Count < 1)
                StartTimer();
            
            _playerSequence.Add(new SequenceElement(index, _timer));
            
            var lastIndex = _playerSequence.Count - 1;
            
            _barController.AddLine(_timer);

            if (_playerSequence[lastIndex].Index != _sequence[lastIndex].Index)
            {
                _success = false;
                _feedback.ShowFeedback("WRONG!", 0.25f);
            }
            else
            {
                var diff = Mathf.Abs(_playerSequence[lastIndex].Time - _sequence[lastIndex].Time);
                if (diff < 0.1f)
                {
                    _feedback.ShowFeedback("Perfect!", 0.25f);
                    _barController.ActivateMushroom(lastIndex);
                }
                else if (diff < 0.25f)
                {
                    _feedback.ShowFeedback("Good", 0.25f);
                    _barController.ActivateMushroom(lastIndex);
                }
                else
                {
                    _success = false;
                    _feedback.ShowFeedback("WRONG!", 0.25f);
                }
            }

            if (_playerSequence.Count == _sequence.Count)
            {
                _completed = true;
                StartCoroutine(FinishRoutine());
            }
        }
#endregion
        
#region Event Functions
        private void Subscribe()
        {
            InputController.Instance.Pressed += OnPressed;
        }

        private void Unsubscribe()
        {
            InputController.Instance.Pressed -= OnPressed;
        }

        private void OnPressed(Vector3 pos)
        {
            if (_completed || _sequencePlaying)
                return;
            
            var index = _mushroomController.GetMushroomAtPoint(pos, out var mushroom);
            if (index == -1)
                return;

            MushroomOnPressed(index);
        }
        public void OnFinish()
        {
            Unsubscribe();
            _sequence.Clear();
            _playerSequence.Clear();
        }

#endregion

#region Coroutines

        private IEnumerator PlaySequence()
        {
            _sequencePlaying = true;
            _success = true;
            _feedback.ShowFeedback("Get Ready", 1f);
            yield return new WaitForSecondsRealtime(2f);
            _barController.ToggleTimerLine(true);
            _timer = 0f;
            _timerRunning = true;
            
            for (int i = 0; i < _sequence.Count; i++)
            {
                var seq = _sequence[i];
                _mushroomController.PlayMushroom(seq.Index);
                _barController.ActivateMushroom(i);
                if (i == _sequence.Count - 1)
                    break;
                
                var duration = _sequence[i+1].Time - seq.Time;
                yield return new WaitForSecondsRealtime(duration);
            }

            _timerRunning = false;
            _barController.ToggleTimerLine(false);
            yield return new WaitForSeconds(StartDelay);
            _barController.DisableMushrooms();
            _feedback.ShowFeedback("GO!", 0.5f);
            _sequencePlaying = false;
        }

        private IEnumerator FinishRoutine()
        {
            //_feedback.ShowFeedback(_success ? "Success" : "Failed", 0.8f);
            yield return new WaitForSecondsRealtime(1f);
            _barController.Active = false;
            Debug.Log("Completed");
            GameManager.Instance.FinishLevel(_success);
        }

#endregion
    }
}

