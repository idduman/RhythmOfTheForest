using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;

namespace Witmina_rotf
{
    public class Mushroom : MonoBehaviour
    {
        [SerializeField] private int _materialIndex = 1;
        [SerializeField] private float _lowEmissionIntensity = -0.5f;
        [SerializeField] private float _lowLightIntensity = 1f;
        private Light _light;
        private Renderer _renderer;
        private float _litLightIntensity;
        private float _litEmissionIntensity;
        private Color _litEmissionColor;

        private Coroutine _lightRoutine;

        private const byte k_MaxByteForOverexposedColor = 191; //internal Unity const

        private void Awake()
        {
            _light = GetComponentInChildren<Light>();
            if (!_light)
            {
                Debug.Log($"No Light found in mushroom {name}");
                return;
            }

            _renderer = GetComponentInChildren<Renderer>();
            if (!_renderer)
            {
                Debug.Log($"No Renderer found in mushroom {name}");
                return;
            }

            _litLightIntensity = _light.intensity;
            _litEmissionColor = _renderer.materials[_materialIndex].GetColor("_EmissionColor");
            _litEmissionIntensity = GetHDRColorIntensity(_litEmissionColor);

            _light.intensity = _lowLightIntensity;
            _renderer.materials[1].SetColor("_EmissionColor",
                ChangeHDRColorIntensity(_litEmissionColor, _lowEmissionIntensity));

        }

        public void LightUp()
        {
            if(_lightRoutine != null)
                StopCoroutine(_lightRoutine);

            _lightRoutine = StartCoroutine(LightRoutine());
        }

        private float GetHDRColorIntensity(Color hdrColor)
        {
            float maxColorComponent = hdrColor.maxColorComponent;
            float scaleFactorToGetIntensity = k_MaxByteForOverexposedColor / maxColorComponent;
            return Mathf.Log(255f / scaleFactorToGetIntensity) / Mathf.Log(2f);
        }
        
        private Color ChangeHDRColorIntensity(Color hdrColor, float intensity)
        {
            // Get color intensity
            var currentIntensity = GetHDRColorIntensity(hdrColor);

            // Get original color with ZERO intensity
            float currentScaleFactor = Mathf.Pow(2, currentIntensity);
            Color originalColorWithoutIntensity = hdrColor / currentScaleFactor;
            
            // Set color intensity
            float newScaleFactor = Mathf.Pow(2, intensity);
            Color colorToRetun = originalColorWithoutIntensity * newScaleFactor;

            // Return color
            return colorToRetun;
        }

        private IEnumerator LightRoutine()
        {
            transform.DOKill();
            transform.DOPunchScale(0.2f * Vector3.one, 0.2f);

            _light.intensity = _litLightIntensity;
            _renderer.materials[_materialIndex].SetColor("_EmissionColor",
                ChangeHDRColorIntensity(_litEmissionColor, _litEmissionIntensity));

            yield return new WaitForSeconds(0.15f);
            
            _light.intensity = _lowLightIntensity;
            _renderer.materials[_materialIndex].SetColor("_EmissionColor",
                ChangeHDRColorIntensity(_litEmissionColor, _lowEmissionIntensity));
        }
    }
}
