using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Witmina_rotf
{
    public class InputController : SingletonBehaviour<InputController>
    {
        [SerializeField] private float _moveTreshold = 0.05f;
        public event Action<Vector3> Pressed;
        public event Action<Vector3> Moved;
        public event Action<Vector3> Released;
        
        private Vector3 _previousPos;
        void Update()
        {
            var inputPos = Input.mousePosition;
            if (Input.GetMouseButtonDown(0))
            {
                _previousPos = inputPos;
                Pressed?.Invoke(inputPos);
            }
            if (Input.GetMouseButton(0))
            {
                if (Vector3.Distance(_previousPos, inputPos) > _moveTreshold)
                    Moved?.Invoke(inputPos);
            }
            if (Input.GetMouseButtonUp(0))
            {
                Released?.Invoke(inputPos);
            }
        }
    }
}

