using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Witmina_rotf
{
    public class MushroomController : MonoBehaviour
    {
        [SerializeField] private List<Mushroom> _mushrooms;
        private List<SequenceElement> _sequence;

        private Camera _camera;

        private void Awake()
        {
            _camera = Camera.main;
        }

        public int GetMushroomAtPoint(Vector3 point, out Mushroom mushroom)
        {
            mushroom = null;
            var ray = _camera.ScreenPointToRay(point);
            if(!Physics.Raycast(ray, out var hit, 100f, Physics.AllLayers)
               || !hit.collider.TryGetComponent<Mushroom>(out mushroom)
               || !_mushrooms.Contains(mushroom)) 
                    return -1;

            return _mushrooms.IndexOf(mushroom);
        }

        public void PlayMushroom(int index)
        {
            _mushrooms[index].LightUp();
            AudioController.Instance.PlayMushroomSound(index);
        }
    }
}

