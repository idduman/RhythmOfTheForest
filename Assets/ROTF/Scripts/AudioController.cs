using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Witmina_rotf
{
    public class AudioController : SingletonBehaviour<AudioController>
    {
        [SerializeField] private List<AudioSource> _mushroomNotes;
        public void PlayMushroomSound(int id)
        {
            _mushroomNotes[id].Play();
        }

        public void PlaySound(SoundType soundType)
        {
            switch (soundType)
            {
                case SoundType.Start:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(soundType), soundType, null);
            }
        }
    }

    public enum SoundType
    {
        Start,
    }
}

